/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
package org.vergien.containerpool;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.Container;
import org.testcontainers.lifecycle.Startable;
import org.vergien.containerpool.pool.PooledContainerFactory;

public class ContainerPool<T extends Container<T> & Startable> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContainerPool.class);
    private static final ConcurrentMap<String, ContainerPool<?>> INSTANCES = new ConcurrentHashMap<>();

    private final ObjectPool<T> pool;
    private final ThreadLocal<T> threadLocalContainer = new ThreadLocal<>();
    private final String key;

    private ContainerPool(PooledContainerFactory<T> containerFactory, int maximumContainers, String key) {
        GenericObjectPoolConfig<T> config = new GenericObjectPoolConfig<>();
        if (maximumContainers >= 0) {
            config.setMaxTotal(maximumContainers);
        }
        this.pool = new GenericObjectPool<>(containerFactory, config);
        this.key = key;
    }

    public T borrowContainer() throws Exception {
        LOGGER.info("Borrow container for extension: {}", key);
        T container = pool.borrowObject();
        threadLocalContainer.set(container);
        return container;
    }

    public void returnContainer(T container) throws Exception {
        LOGGER.info("Return container for extension: {}", key);
        threadLocalContainer.remove();
        pool.returnObject(container);
    }

    public synchronized static <T extends Container<T> & Startable> ContainerPool<T> getInstance(String key,
                                                                                                 Class<T> containerClass) {
        LOGGER.debug("Getting pool for extension {}", key);
        return (ContainerPool<T>) INSTANCES.get(key);
    }

    public synchronized static <T extends Container<T> & Startable> ContainerPool<T> getInstance(String key) {
        LOGGER.debug("Getting pool for extension {}", key);
        return (ContainerPool<T>) INSTANCES.get(key);
    }

    public synchronized static <T extends Container<T> & Startable> ContainerPool<T> createInstance(String key,
                                                                                                    PooledContainerFactory<T> containerFactory, int maximumContainers) {
        LOGGER.debug("Creating pool for extension {}", key);
        if (!INSTANCES.containsKey(key)) {
            INSTANCES.put(key, new ContainerPool(containerFactory, maximumContainers, key));
        }
        return (ContainerPool<T>) INSTANCES.get(key);
    }

    public T getThreadLocalContainer() {
        return threadLocalContainer.get();
    }
}
