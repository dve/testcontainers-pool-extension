/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
package org.vergien.containerpool;

import java.util.function.Supplier;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.testcontainers.containers.Container;
import org.testcontainers.lifecycle.Startable;
import org.vergien.containerpool.pool.PooledContainerFactory;

/**
 * Base of the Container Pool Extension.
 * <p>
 * An implementation of this is needed to create a specific implementation for one kind of a container.
 * A container is provided to every test as a parameter. Optional a container is provided to every test, this is controlled my the containerForEveryTest constructor property.
 * @param <T> the type of the {@link Container}
 * @see ParameterResolver
 */
public abstract class ContainerPoolExtension<T extends Container<T> & Startable>
        implements BeforeEachCallback, AfterEachCallback, ParameterResolver {
    private final Class<T> containerClass;
    private final PooledContainerFactory<T> pooledObjectFactory;
    private final String key;
    private final ThreadLocal<T> threadLocalContainer = new ThreadLocal<>();
    private final boolean containerForEveryTest;

    /**
     * Constructs a new {@link ContainerPoolExtension} with the default pool size of the underlying pool implementation.
     * @param containerClass the class of the containers of this extension
     * @param containerSupplier the supplier providing new {@link Container} objects
     * @param containerForEveryTest set to true if the extension should provide a container for every test, not only as a parameter
     * @see GenericObjectPoolConfig
     */
    public ContainerPoolExtension(Class<T> containerClass, Supplier<T> containerSupplier,
                                  boolean containerForEveryTest) {
        this(containerClass, -1, containerSupplier, containerForEveryTest);
    }

    /**
     * Constructs a new {@link ContainerPoolExtension} with the given pool size.
     * @param containerClass the class of the containers of this extension
     * @param containerSupplier the supplier providing new {@link Container} objects
     * @param maximumContainers the maximum number of containers which are created
     * @param containerForEveryTest set to true if the extension should provide a container for every test, not only as a parameter
     * @see GenericObjectPoolConfig
     */
    public ContainerPoolExtension(Class<T> containerClass, int maximumContainers, Supplier<T> containerSupplier,
                                  boolean containerForEveryTest) {
        this.containerClass = containerClass;
        this.pooledObjectFactory = new PooledContainerFactory<>(containerSupplier);
        this.key = createKey();
        this.containerForEveryTest = containerForEveryTest;
        ContainerPool.createInstance(key, this.pooledObjectFactory, maximumContainers);
    }

    protected String createKey() {
        return this.getClass().getCanonicalName();
    }

    @Override
    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        return parameterContext.getParameter().getType() == containerClass;
    }

    @Override
    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext)
            throws ParameterResolutionException {
        try {
            borrowContainer();
        }
        catch (Exception e) {
            throw new ParameterResolutionException("Failure getting container", e);
        }
        return threadLocalContainer.get();
    }

    @Override
    public void afterEach(ExtensionContext context) throws Exception {
        T container = threadLocalContainer.get();
        threadLocalContainer.remove();
        if (container != null) {
            ((ContainerPool<T>) ContainerPool.getInstance(key)).returnContainer(container);
        }
    }

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        if (containerForEveryTest) {
            borrowContainer();
        }
    }

    private void borrowContainer() throws Exception {
        var pool = ContainerPool.getInstance(key, containerClass);
        if (threadLocalContainer.get() == null) {
            threadLocalContainer.set(pool.borrowContainer());
        }
    }

    public static <T extends Container<T> & Startable> T getThreadLocalContainer(Class<?> extensionClass) {
        var pool = ContainerPool.getInstance(extensionClass.getCanonicalName());
        return (T) pool.getThreadLocalContainer();

    }

}
