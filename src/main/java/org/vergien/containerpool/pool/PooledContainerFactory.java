/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
package org.vergien.containerpool.pool;

import java.util.function.Supplier;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.testcontainers.containers.Container;
import org.testcontainers.lifecycle.Startable;

public class PooledContainerFactory<T extends Container<T> & Startable> extends BasePooledObjectFactory<T> {
    private final Supplier<T> containerSupplier;

    public PooledContainerFactory(Supplier<T> containerpplier) {
        this.containerSupplier = containerpplier;
    }

    @Override
    public T create() throws Exception {
        T container = containerSupplier.get();
        container.start();
        return container;
    }

    @Override
    public PooledObject<T> wrap(T obj) {
        return new DefaultPooledObject<T>(obj);
    }

    @Override
    public void destroyObject(PooledObject<T> p) throws Exception {
        p.getObject().stop();
        super.destroyObject(p);
    }
}
