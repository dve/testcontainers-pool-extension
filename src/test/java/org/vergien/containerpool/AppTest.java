/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
package org.vergien.containerpool;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vergien.containerpool.container.HelloWorldContainer;
import org.vergien.containerpool.container.OnlyParameterContainer;
import org.vergien.containerpool.container.OtherHelloWorldContainer;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(DefaultHelloWorldContainerPoolExtension.class)
@ExtendWith(LimitedHelloWorldContainerPoolExtension.class)
@ExtendWith(OnlyParameterResolverExtension.class)
class AppTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppTest.class);

    @Test
    @DisplayName("Verify that containers are injected as parameters")
    void test001(HelloWorldContainer container, OtherHelloWorldContainer otherContainer) {
        LOGGER.info("HelloWorldContainer: {}\nOtherHelloWorldContainer: {}", container.getContainerId(), otherContainer.getContainerId());
        assertThat(container.getContainerId()).isNotBlank();
        assertThat(otherContainer.getContainerId()).isNotBlank();

    }

    @Test
    @DisplayName("Verify that containers are injected as parameters, also only parameter extension")
    void test003(HelloWorldContainer container, OtherHelloWorldContainer otherContainer, OnlyParameterContainer onlyParameterContainer) {
        LOGGER.info("HelloWorldContainer: {}\nOtherHelloWorldContainer: {}", container.getContainerId(), otherContainer.getContainerId());
        assertThat(container.getContainerId()).isNotBlank();
        assertThat(otherContainer.getContainerId()).isNotBlank();
        assertThat(onlyParameterContainer.getContainerId()).isNotBlank();
    }

    @Test
    @DisplayName("Verify that containers are injected as ThreadLocal")
    void test002() {
        HelloWorldContainer container = DefaultHelloWorldContainerPoolExtension.getThreadLocalContainer();
        OtherHelloWorldContainer otherContainer = LimitedHelloWorldContainerPoolExtension.getThreadLocalContainer();
        OnlyParameterContainer onlyParameterContainer = OnlyParameterResolverExtension.getThreadLocalContainer();
        LOGGER.info("HelloWorldContainer: {}\nOtherHelloWorldContainer: {}",
                container.getContainerId(),
                otherContainer.getContainerId());
        assertThat(container.getContainerId()).isNotBlank();
        assertThat(otherContainer.getContainerId()).isNotBlank();
        assertThat(onlyParameterContainer).isNull();
    }
}
