/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
package org.vergien.containerpool;

import org.vergien.containerpool.container.HelloWorldContainer;

public class DefaultHelloWorldContainerPoolExtension extends ContainerPoolExtension<HelloWorldContainer> {

    public DefaultHelloWorldContainerPoolExtension() {
        super(HelloWorldContainer.class, HelloWorldContainer::new, true);

    }

    public static HelloWorldContainer getThreadLocalContainer() {
        return ContainerPoolExtension.getThreadLocalContainer(DefaultHelloWorldContainerPoolExtension.class);
    }

}
