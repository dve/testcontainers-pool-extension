/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
package org.vergien.containerpool;

import org.vergien.containerpool.container.OtherHelloWorldContainer;

public class LimitedHelloWorldContainerPoolExtension extends ContainerPoolExtension<OtherHelloWorldContainer> {

    public LimitedHelloWorldContainerPoolExtension() {
        super(OtherHelloWorldContainer.class, 1, OtherHelloWorldContainer::new, true);

    }

    public static OtherHelloWorldContainer getThreadLocalContainer() {
        return ContainerPoolExtension.getThreadLocalContainer(LimitedHelloWorldContainerPoolExtension.class);
    }

}
