/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
package org.vergien.containerpool;

import org.vergien.containerpool.container.OnlyParameterContainer;

public class OnlyParameterResolverExtension extends ContainerPoolExtension<OnlyParameterContainer> {
    public OnlyParameterResolverExtension() {
        super(OnlyParameterContainer.class, OnlyParameterContainer::new, false);
    }

    public static OnlyParameterContainer getThreadLocalContainer() {
        return ContainerPoolExtension.getThreadLocalContainer(OnlyParameterResolverExtension.class);
    }
}
