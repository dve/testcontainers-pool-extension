/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
package org.vergien.containerpool.container;

import org.testcontainers.containers.GenericContainer;

public class OnlyParameterContainer extends GenericContainer<OnlyParameterContainer> {
    public OnlyParameterContainer() {
        super("hello-world");
    }
}
