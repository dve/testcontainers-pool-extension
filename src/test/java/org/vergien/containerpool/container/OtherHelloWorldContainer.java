/*
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Copyright The original authors
 *
 *  Licensed under the Apache Software License version 2.0, available at http://www.apache.org/licenses/LICENSE-2.0
 */
package org.vergien.containerpool.container;

import org.testcontainers.containers.GenericContainer;

public class OtherHelloWorldContainer extends GenericContainer<OtherHelloWorldContainer> {
    public OtherHelloWorldContainer() {
        super("hello-world");
    }

}
